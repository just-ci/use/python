## [1.6.1](https://gitlab.com/just-ci/use/python/compare/v1.6.0...v1.6.1) (2024-09-01)


### Bug Fixes

* flake8 config file ([da2fd1d](https://gitlab.com/just-ci/use/python/commit/da2fd1d12b07d846dd27d0d46dc85ed4224014b7))

# [1.6.0](https://gitlab.com/just-ci/use/python/compare/v1.5.0...v1.6.0) (2024-05-25)


### Features

* additional preferred config ([e46c286](https://gitlab.com/just-ci/use/python/commit/e46c286b8d6de666fa5ce7e31cd4eeac57be3c6b))

# [1.5.0](https://gitlab.com/just-ci/use/python/compare/v1.4.0...v1.5.0) (2022-12-27)


### Features

* add recommended ci template ([a58a0b6](https://gitlab.com/just-ci/use/python/commit/a58a0b68f3ab8800cda5bfc50baf135cdd782086))

# [1.4.0](https://gitlab.com/just-ci/use/python/compare/v1.3.2...v1.4.0) (2022-12-27)


### Features

* add example logging ([165c966](https://gitlab.com/just-ci/use/python/commit/165c9660d32ddcd02a4be15a7a473794386140ec))
* example __init__.py + version check ([f39179f](https://gitlab.com/just-ci/use/python/commit/f39179fcaff2ae2a6be3097a5354d82e395c0c2d))

## [1.3.2](https://gitlab.com/just-ci/use/python/compare/v1.3.1...v1.3.2) (2022-12-14)


### Bug Fixes

* venv creation ([40b56bc](https://gitlab.com/just-ci/use/python/commit/40b56bc176b74ca699c713daaf355a040209d168))

## [1.3.1](https://gitlab.com/just-ci/use/python/compare/v1.3.0...v1.3.1) (2022-12-14)


### Bug Fixes

* creating src dir and installing venv ([4492b0e](https://gitlab.com/just-ci/use/python/commit/4492b0e3346a4b523632d4ac16e464fb206c3353))

# [1.3.0](https://gitlab.com/just-ci/use/python/compare/v1.2.0...v1.3.0) (2022-12-14)


### Features

* add gitignore to example ([8d1953c](https://gitlab.com/just-ci/use/python/commit/8d1953c2e1d50cc9bafe11ac1b2622e8f5a438dc))

# [1.2.0](https://gitlab.com/just-ci/use/python/compare/v1.1.0...v1.2.0) (2022-12-14)


### Bug Fixes

* example dir creation ([087b8b1](https://gitlab.com/just-ci/use/python/commit/087b8b166e58de82cabd0880e4f6fb44187e0ebb))
* example url ([c4bcf75](https://gitlab.com/just-ci/use/python/commit/c4bcf75df629a38f537bea4bcd58d8bcb4fdad5c))


### Features

* base src dir + open vs code ([03bc0be](https://gitlab.com/just-ci/use/python/commit/03bc0be294d1c46c268d0e1511be5f654d14935f))

# [1.1.0](https://gitlab.com/just-ci/use/python/compare/v1.0.0...v1.1.0) (2022-12-14)


### Features

* run vs code when done ([0100531](https://gitlab.com/just-ci/use/python/commit/010053144d5ad6a7967c9c959385b1929cce5488))

# 1.0.0 (2022-12-13)


### Bug Fixes

* various changes ([fc0c24f](https://gitlab.com/just-ci/use/python/commit/fc0c24f8f53d99503a2645f94fc1fcbabbce6679))
