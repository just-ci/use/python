import argparse
import json
import os
import subprocess  # nosec
import sys
from pathlib import Path
from shutil import which
from urllib.request import urlopen, urlretrieve

CURRENT_VERSION = "v1.6.1"
BASE_URL = f"https://gitlab.com/just-ci/use/python/-/raw/{CURRENT_VERSION}/"
PROJECT_ID = "41821996"

DEV_PACKAGES = [
    "black",
    "pytest",
    "pytest-cov",
    "flake8",
    "isort",
    "bandit",
    "mypy",
    "pylint",
    "pycln",
    "pyupgrade",
]
POETRY_ADDITIONAL_TOML = """
[tool.pylint]
disable = [
    "missing-module-docstring",
    "missing-function-docstring",
    "logging-fstring-interpolation",
]

[tool.tbump]

[tool.tbump.version]
current = "0.1.0"

regex = '''
  (?P<major>\d+)
  \.
  (?P<minor>\d+)
  \.
  (?P<patch>\d+)
  '''

[tool.tbump.git]
message_template = "Bump to {new_version}"
tag_template = "v{new_version}"

[[tool.tbump.file]]
src = "pyproject.toml"
search = 'version = "{current_version}"'
"""

SRC_FILES = ["src/__main__.py", "src/main.py", "src/__init__.py", ".gitignore"]
RECOMMENDED_CI_PROJECT_ID = "30777660"
RECOMMENDED_CI = """---
include:
  - remote: https://jobs.just-ci.dev/LATEST_TAG/templates/python.yml
"""
FLAKE8_CONFIG = """[flake8]
max-line-length = 88
"""


def get_latest_tag(project_id: str) -> str:
    url = f"https://gitlab.com/api/v4/projects/{project_id}/repository/tags?order_by=version"
    with urlopen(url) as url_data:  # nosec
        data = json.load(url_data)
    latest_tag: str = data[0]["name"]
    return latest_tag


def parse_args(argv: list[str] = sys.argv[1:]) -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    def empty_or_non_existing_dir(string: str) -> Path:
        path = Path(string).absolute()
        if path.exists():
            if path.is_file():
                parser.error(f"{string} already exists and is a file.")
        return path

    parser.add_argument(
        "--allow-root", help="Allow running as root.", action="store_true"
    )
    parser.add_argument(
        "--no-src",
        help="Do not add basic src dir.",
        action="store_true",
    )
    parser.add_argument(
        "--no-ci",
        help="Do not add Gitlab CI recommended template.",
        action="store_true",
    )
    parser.add_argument(
        "package",
        help="Target directory for your package. Use . for current directory. "
        "Directory will be created if it doesn't exist.",
        type=empty_or_non_existing_dir,
    )
    parsed_args = parser.parse_args(argv)

    return parsed_args


args = parse_args()
if (latest_tag := get_latest_tag(PROJECT_ID)) != CURRENT_VERSION:
    print(f"[!] A newer version of this script is available: {latest_tag}.")

target_dir = Path(args.package)
dir_name = target_dir.absolute().parts[-1]
package_name = dir_name.replace("-", "_")

package_dir = target_dir / package_name
package_dir.mkdir(exist_ok=True, parents=True)

if (pyprojecttoml := (target_dir / "pyproject.toml")).exists():
    print(f"{pyprojecttoml} already exists.")
    sys.exit(1)


if hasattr(os, "geteuid"):  # Windows doesn't support this
    if os.geteuid() == 0 and not args.allow_root:
        print("[!] Don't run me as root. Set --allow-root to run this as root anyway.")
        sys.exit(1)
else:
    print("[-] Skipping root check on unsupported platform")

if not (poetry_path := which("poetry")):
    print("[!] poetry not installed. Installing it for you now.")
    urlretrieve(
        "https://install.python-poetry.org",
        poetry_installer := Path("/tmp/poetry_install.py"),
    )
    POETRY_INSTALL_COMMAND = f"cat {poetry_installer} | python3 -"
    subprocess.run(POETRY_INSTALL_COMMAND, shell=True, check=True)  # nosec
    poetry_installer.unlink()
    poetry_path = "~/.local/bin/poetry"
else:
    print("[+] Ensuring poetry is at latest")
    subprocess.run(f"{poetry_path} self update", shell=True, check=True)

target_dir.mkdir(parents=True, exist_ok=True)

POETRY_ADDITIONAL_TOML += f"""
[[tool.tbump.file]]
src = "{package_name}/__init__.py"
"""

print("[+] Creating pyproject.toml")
subprocess.run(
    f"{poetry_path} init -n", shell=True, check=True, cwd=target_dir
)  # nosec

print("[+] Adding additional content to pyproject.toml")
with open(target_dir / "pyproject.toml", "a+", encoding="utf-8") as file_stream:
    file_stream.write(POETRY_ADDITIONAL_TOML)

print("[+] Adding .flake8 config file")
with open(target_dir / ".flake8", "w", encoding="utf-8") as file_stream:
    file_stream.write(FLAKE8_CONFIG)

print(f"[+] Installing dev packages: {' '.join(DEV_PACKAGES)}")
subprocess.run(
    f"{poetry_path} add --group dev {' '.join(DEV_PACKAGES)}",
    check=True,
    shell=True,
    cwd=target_dir,  # nosec
)

if not (readme := Path(target_dir / "README.md")).exists():
    print("[*] Creating empty README.md...", end="")
    open(readme, "a").close()
    print(" done.")


if args.no_src:
    open(package_dir / "__main__.py", "a", encoding="utf-8").close()
else:
    print(f"[*] Creating {package_name} subdir with basic code...")
    for file in SRC_FILES:
        urlretrieve(  # nosec
            f"{BASE_URL}example/{file}", target_dir / file.replace("src", package_name)
        )

    with open(
        target_dir / package_name / "__main__.py", "r+", encoding="utf-8"
    ) as file_stream:
        content = file_stream.read()
        content = content.replace("example.main", f"{package_name}.main")
        file_stream.seek(0)
        file_stream.write(content)
        file_stream.truncate()

if not args.no_ci:
    if (target_file := target_dir / ".gitlab-ci.yml").exists():
        print(f"[-] {target_file} already exists.")
    else:
        latest_ci_tag = get_latest_tag(RECOMMENDED_CI_PROJECT_ID)
        recommended_ci = RECOMMENDED_CI.replace("LATEST_TAG", latest_ci_tag)
        with open(target_file, "w") as file_stream:
            file_stream.write(recommended_ci)


subprocess.run(
    f"{poetry_path} install", shell=True, check=True, cwd=target_dir
)  # nosec
print("[+] Your package dir is ready.")

if which("code"):
    print("[*] Opening VS Code...")
    try:
        subprocess.run("code .".split(), cwd=target_dir)  # nosec
    except OSError:
        print(f"Failed to open VS Code. Try it yourself using: 'code {target_dir}'")
else:
    extra_cmd = ""
    if Path(".") != target_dir:
        extra_cmd = f"cd {target_dir} && "
    print(f"[+] Run '{extra_cmd}poetry shell' to activate the new virtualenv.")
