# Python init script

Run the following to read the help on how to initialize a directory with a
recommended python environment. Ready to build your new package.

```shell
curl -sSLf https://gitlab.com/just-ci/use/python/-/raw/v1.6.1/init.py | python - -h
```

This script will:

- Create a pyproject.toml based on poetry
- Add various recommended development packages
- Create a recommended directory structure
- Add example code to get started (argparse and logging)

## Examples

Create a new project called `hello-world` in a subfolder:

```shell
curl -sSLf https://gitlab.com/just-ci/use/python/-/raw/v1.6.1/init.py | python - hello-world
```

Create a new project in the current folder, using the name of that folder:

```shell
curl -sSLf https://gitlab.com/just-ci/use/python/-/raw/v1.6.1/init.py | python - .
```

Quite simple.
