import logging
from argparse import Namespace

logger = logging.getLogger(__name__)


def main(args: Namespace) -> None:
    logger.info("[*] Running main...")
    # Do your thing here
