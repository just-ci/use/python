import argparse
import logging
import sys

from example.main import main


def parse_args(argv: list[str] = sys.argv[1:]) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose", help="Enable debug level logging.", action="store_true"
    )

    parsed_args = parser.parse_args(argv)
    return parsed_args


if __name__ == "__main__":
    args = parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    main(args)
